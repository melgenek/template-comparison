package comparison

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver

import scala.concurrent.ExecutionContextExecutor

object ThymeleafMain {
	implicit val system = ActorSystem("example")
	implicit val materializer = ActorMaterializer()
	implicit val executionContext: ExecutionContextExecutor = system.dispatcher

	private val templateEngine: TemplateEngine = {
		val cl = Thread.currentThread.getContextClassLoader
		val engine = new TemplateEngine()
		val resolver = new ClassLoaderTemplateResolver(cl)
		resolver.setPrefix("/templates/")
		engine.setTemplateResolver(resolver)
		engine
	}

	def route: Route = pathEndOrSingleSlash {
		get {
			complete {
				val context = new Context()
				context.setVariable("login", "azaza")
				context.setVariable("password", "pass")
				HttpEntity(ContentTypes.`text/html(UTF-8)`, templateEngine.process("hello.html", context))
			}
		}
	}

	def main(args: Array[String]): Unit = {
		Http().bindAndHandle(route, "0.0.0.0", 8989)
	}

}
