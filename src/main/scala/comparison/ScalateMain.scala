package comparison

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import org.fusesource.scalate.TemplateEngine

import scala.concurrent.ExecutionContextExecutor
import scala.io.Source

object ScalateMain {
  implicit val system = ActorSystem("example")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val engine = new TemplateEngine

  private val page = {
    val template = Source.fromFile(JMustacheMain.getClass.getClassLoader.getResource("template.mustache").toURI).getLines().mkString
    val mustache = engine.compileMoustache(template)
    mustache.source
  }

  case class Context(login: String, password: String)

  def route: Route = pathEndOrSingleSlash {
    get {
      complete {
        val context = Map(
          "login" -> "azaza",
          "password" -> "pass"
        )

        HttpEntity(ContentTypes.`text/html(UTF-8)`, engine.layout(page, context))
      }
    }
  }

  def main(args: Array[String]): Unit = {
    Http().bindAndHandle(route, "0.0.0.0", 8989)
  }

}
