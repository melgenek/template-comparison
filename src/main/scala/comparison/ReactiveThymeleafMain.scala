package comparison

import java.nio.charset.Charset

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Source, StreamConverters}
import akka.util.ByteString
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver

import scala.concurrent.{ExecutionContextExecutor, Future}

object ReactiveThymeleafMain {
	implicit val system = ActorSystem("example")
	implicit val materializer = ActorMaterializer()
	implicit val executionContext: ExecutionContextExecutor = system.dispatcher

	private val templateEngine: TemplateEngine = {
		val cl = Thread.currentThread.getContextClassLoader
		val engine = new TemplateEngine()
		val resolver = new ClassLoaderTemplateResolver(cl)
		resolver.setPrefix("/templates/")
		engine.setTemplateResolver(resolver)
		engine
	}

	def route: Route = pathEndOrSingleSlash {
		get {
			complete {
				val context = new Context()
				context.setVariable("login", "azaza")
				context.setVariable("password", "pass")
				val throttled = templateEngine.processThrottled("hello.html", context)
				val byteSource: Source[ByteString, Unit] = StreamConverters.asOutputStream()
					.mapMaterializedValue(os => Future {
						while (!throttled.isFinished)
							throttled.process(1024, os, Charset.forName("UTF-8"))
						os.close()
					})
				HttpEntity(ContentTypes.`text/html(UTF-8)`, byteSource)
			}
		}
	}

	def main(args: Array[String]): Unit = {
		Http().bindAndHandle(route, "0.0.0.0", 8989)
	}

}

import java.nio.charset.Charset

import org.thymeleaf.IThrottledTemplateProcessor

class StreamThrottledTemplateProcessor(val throttledProcessor: IThrottledTemplateProcessor) {

	this.chunkCount = -1 // First chunk will be considered number 0

	this.totalBytesProduced = 0L
	private var chunkCount = 0
	private var totalBytesProduced = 0L

	def process(maxOutputInBytes: Int, outputStream: Nothing, charset: Charset): Int = {
		val chunkBytes = this.throttledProcessor.process(maxOutputInBytes, outputStream, charset)
		this.totalBytesProduced += chunkBytes
		chunkBytes
	}

	def getProcessorIdentifier: String = this.throttledProcessor.getProcessorIdentifier

	def isFinished: Boolean = this.throttledProcessor.isFinished

	def startChunk(): Unit = {
		this.chunkCount += 1
	}

	def getChunkCount: Int = this.chunkCount

	def getTotalBytesProduced: Long = this.totalBytesProduced

}