package comparison

import java.io.{OutputStreamWriter, PipedInputStream, PipedOutputStream}
import java.util

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.{ActorMaterializer, IOResult}
import akka.stream.scaladsl.{Source, StreamConverters}
import akka.util.ByteString
import com.github.mustachejava.DefaultMustacheFactory

import scala.concurrent.{ExecutionContextExecutor, Future}

object MustacheMain {
	implicit val system = ActorSystem("example")
	implicit val materializer = ActorMaterializer()
	implicit val executionContext: ExecutionContextExecutor = system.dispatcher

	private val page = {

		val mf = new DefaultMustacheFactory
		val mustache = mf.compile("template.mustache")
		mustache
	}

	case class Context(login: String, password: String)

	def route: Route = pathEndOrSingleSlash {
		get {
			complete {
				val context = new util.HashMap[String, String]()
				context.put("login", "azaza")
				context.put("password", "pass")

				val in = new PipedInputStream
				val os = new PipedOutputStream(in)
				val writer = new OutputStreamWriter(os)
				page.execute(writer, context)
				writer.close()

				HttpEntity(ContentTypes.`text/html(UTF-8)`, StreamConverters.fromInputStream(() => in))
			}
		}
	}

	def main(args: Array[String]): Unit = {
		Http().bindAndHandle(route, "0.0.0.0", 8989)
	}

}
