package comparison

import java.util

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import org.thymeleaf.cache.{AbstractCacheManager, ExpressionCacheKey, ICache, TemplateCacheKey}
import org.thymeleaf.{IEngineConfiguration, TemplateEngine}
import org.thymeleaf.context.Context
import org.thymeleaf.engine.TemplateModel
import org.thymeleaf.templateresolver.{AbstractConfigurableTemplateResolver, ClassLoaderTemplateResolver}
import org.thymeleaf.templateresource.{ITemplateResource, UrlTemplateResource}

import scala.concurrent.ExecutionContextExecutor

class CustomCacheManager extends AbstractCacheManager {

	override def initializeExpressionCache(): ICache[ExpressionCacheKey, AnyRef] = ???

	override def initializeTemplateCache(): ICache[TemplateCacheKey, TemplateModel] = ???

}

class CustomResolver extends AbstractConfigurableTemplateResolver {
	override def computeTemplateResource(configuration: IEngineConfiguration,
																			 ownerTemplate: String,
																			 template: String,
																			 resourceName: String,
																			 characterEncoding: String,
																			 templateResolutionAttributes: util.Map[String, AnyRef]): ITemplateResource =
		new UrlTemplateResource(s"https://gitlab.com/melgenek/template-comparison/raw/master/src/main/resources/templates/$resourceName", characterEncoding)

}

object CustomThymeleafMain {
	implicit val system = ActorSystem("example")
	implicit val materializer = ActorMaterializer()
	implicit val executionContext: ExecutionContextExecutor = system.dispatcher

	private val templateEngine: TemplateEngine = {
		val engine = new TemplateEngine()
		val resolver = new CustomResolver()
		//    engine.setCacheManager(null)
		engine.setTemplateResolver(resolver)
		engine
	}

	def route: Route = pathEndOrSingleSlash {
		get {
			complete {
				val context = new Context()
				context.setVariable("login", "azaza")
				context.setVariable("password", "pass")
				HttpEntity(
					ContentTypes.`text/html(UTF-8)`,
					templateEngine.process("hello.html", context)
				)
			}
		}
	}

	def main(args: Array[String]): Unit = {
		Http().bindAndHandle(route, "0.0.0.0", 8989)
	}

}
