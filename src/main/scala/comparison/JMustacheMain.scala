package comparison

import java.io.{OutputStreamWriter, PipedInputStream, PipedOutputStream}
import java.util

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.StreamConverters
import comparison.MustacheMain.page
import org.thymeleaf.context.Context

import scala.concurrent.ExecutionContextExecutor
import scala.io.Source

object JMustacheMain {
	implicit val system = ActorSystem("example")
	implicit val materializer = ActorMaterializer()
	implicit val executionContext: ExecutionContextExecutor = system.dispatcher

	private val page = {
		import com.samskivert.mustache.Mustache
		val template = Source.fromFile(JMustacheMain.getClass.getClassLoader.getResource("template.mustache").toURI).getLines().mkString
		Mustache.compiler().compile(template)
	}

	case class Context(login: String, password: String)

	def route: Route = pathEndOrSingleSlash {
		get {
			complete {
				val context = Context("azaza", "pass")
				HttpEntity(
					ContentTypes.`text/html(UTF-8)`,
					page.execute(context)
				)
			}
		}
	}

	def main(args: Array[String]): Unit = {
		Http().bindAndHandle(route, "0.0.0.0", 8989)
	}

}
