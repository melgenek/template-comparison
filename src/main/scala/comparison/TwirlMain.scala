package comparison

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, MediaType}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import org.thymeleaf.context.Context
import play.twirl.api.{Html, Txt, Xml}

import scala.concurrent.ExecutionContextExecutor

case class Foo(login: String, password: String)

object TwirlSupport extends TwirlSupport

trait TwirlSupport {

	/** Serialize Twirl `Html` to `text/html`. */
	implicit val twirlHtmlMarshaller: ToEntityMarshaller[Html] = twirlMarshaller[Html](`text/html`)

	/** Serialize Twirl `Txt` to `text/plain`. */
	implicit val twirlTxtMarshaller: ToEntityMarshaller[Txt] = twirlMarshaller[Txt](`text/plain`)

	/** Serialize Twirl `Xml` to `text/xml`. */
	implicit val twirlXmlMarshaller: ToEntityMarshaller[Xml] = twirlMarshaller[Xml](`text/xml`)

	/** Serialize Twirl formats to `String`. */
	protected def twirlMarshaller[A <: AnyRef : Manifest](contentType: MediaType): ToEntityMarshaller[A] =
		Marshaller.StringMarshaller.wrap(contentType)(_.toString)

}

object TwirlMain {
	implicit val system = ActorSystem("example")
	implicit val materializer = ActorMaterializer()
	implicit val executionContext: ExecutionContextExecutor = system.dispatcher

	import TwirlSupport._

	def route: Route = pathEndOrSingleSlash {
		get {
			complete {
				html.hello.render(Foo("azaza", "pass"))
			}
		}
	}

	def main(args: Array[String]): Unit = {
		Http().bindAndHandle(route, "0.0.0.0", 8989)
	}

}
