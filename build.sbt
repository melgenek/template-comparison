scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
	"com.typesafe.akka" %% "akka-http" % "10.0.11",
	"org.thymeleaf" % "thymeleaf" % "3.0.9.RELEASE",
	"com.github.spullara.mustache.java" % "compiler" % "0.9.5",
	"org.slf4j" % "slf4j-simple" % "1.7.25",
	"com.samskivert" % "jmustache" % "1.14",
	"org.scalatra.scalate" %% "scalate-core" % "1.8.0"
)

enablePlugins(SbtTwirl)

lazy val gatling = project.in(file("gatling"))
	.settings(scalaVersion := "2.12.4")
	.settings(libraryDependencies +=
		"io.gatling.highcharts" % "gatling-charts-highcharts" % "2.3.0"
	)
