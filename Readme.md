200 -> 200-400 -> 400 

### mustache

200 -> 200-400 -> 400 

================================================================================
---- Global Information --------------------------------------------------------
> request count                                      10699 (OK=8183   KO=2516  )
> min response time                                      0 (OK=258    KO=0     )
> max response time                                  57642 (OK=57642  KO=0     )
> mean response time                                  5198 (OK=6796   KO=0     )
> std deviation                                      10122 (OK=11094  KO=0     )
> response time 50th percentile                        675 (OK=1651   KO=0     )
> response time 75th percentile                       4351 (OK=7710   KO=0     )
> response time 95th percentile                      29600 (OK=34029  KO=0     )
> response time 99th percentile                      48025 (OK=49274  KO=0     )
> mean requests/sec                                144.581 (OK=110.581 KO=34    )
---- Response Time Distribution ------------------------------------------------
> t < 800 ms                                          3367 ( 31%)
> 800 ms < t < 1200 ms                                 198 (  2%)
> t > 1200 ms                                         4618 ( 43%)
> failed                                              2516 ( 24%)
---- Errors --------------------------------------------------------------------
> j.n.ConnectException: connection timed out: localhost/127.0.0.   2470 (98.17%)
1:8989
> j.u.c.TimeoutException: Request timeout to localhost/127.0.0.1     46 ( 1.83%)
:8989 after 60000 ms
================================================================================

### thymeleaf

200 -> 200-400 -> 400 

================================================================================
---- Global Information --------------------------------------------------------
> request count                                      10699 (OK=8452   KO=2247  )
> min response time                                      0 (OK=255    KO=0     )
> max response time                                  59912 (OK=59912  KO=0     )
> mean response time                                  5416 (OK=6856   KO=0     )
> std deviation                                       9805 (OK=10575  KO=0     )
> response time 50th percentile                        806 (OK=1715   KO=0     )
> response time 75th percentile                       5170 (OK=8312   KO=0     )
> response time 95th percentile                      29352 (OK=31040  KO=0     )
> response time 99th percentile                      44739 (OK=45965  KO=0     )
> mean requests/sec                                137.167 (OK=108.359 KO=28.808)
---- Response Time Distribution ------------------------------------------------
> t < 800 ms                                          3086 ( 29%)
> 800 ms < t < 1200 ms                                 430 (  4%)
> t > 1200 ms                                         4936 ( 46%)
> failed                                              2247 ( 21%)
---- Errors --------------------------------------------------------------------
> j.n.ConnectException: connection timed out: localhost/127.0.0.   2152 (95.77%)
1:8989
> j.u.c.TimeoutException: Request timeout to localhost/127.0.0.1     95 ( 4.23%)
:8989 after 60000 ms
================================================================================

### thymeleaf throttled

200 -> 200-400 -> 400 

================================================================================
---- Global Information --------------------------------------------------------
> request count                                      10699 (OK=8100   KO=2599  )
> min response time                                      0 (OK=225    KO=0     )
> max response time                                  51871 (OK=51871  KO=0     )
> mean response time                                  4663 (OK=6159   KO=0     )
> std deviation                                       8683 (OK=9506   KO=0     )
> response time 50th percentile                        656 (OK=1694   KO=0     )
> response time 75th percentile                       4326 (OK=7590   KO=0     )
> response time 95th percentile                      26338 (OK=29590  KO=0     )
> response time 99th percentile                      39881 (OK=40078  KO=0     )
> mean requests/sec                                167.172 (OK=126.562 KO=40.609)
---- Response Time Distribution ------------------------------------------------
> t < 800 ms                                          3256 ( 30%)
> 800 ms < t < 1200 ms                                 235 (  2%)
> t > 1200 ms                                         4609 ( 43%)
> failed                                              2599 ( 24%)
---- Errors --------------------------------------------------------------------
> j.n.ConnectException: connection timed out: localhost/127.0.0.   2599 (100.0%)
1:8989
================================================================================

1000 -> 1000-2000 -> 2000 

================================================================================
---- Global Information --------------------------------------------------------
> request count                                      36000 (OK=8156   KO=27844 )
> min response time                                      0 (OK=332    KO=0     )
> max response time                                  59732 (OK=59732  KO=0     )
> mean response time                                  4141 (OK=18277  KO=0     )
> std deviation                                      10409 (OK=14826  KO=0     )
> response time 50th percentile                          0 (OK=15198  KO=0     )
> response time 75th percentile                          0 (OK=27337  KO=0     )
> response time 95th percentile                      30026 (OK=46160  KO=0     )
> response time 99th percentile                      46462 (OK=57471  KO=0     )
> mean requests/sec                                455.696 (OK=103.241 KO=352.456)
---- Response Time Distribution ------------------------------------------------
> t < 800 ms                                           699 (  2%)
> 800 ms < t < 1200 ms                                 115 (  0%)
> t > 1200 ms                                         7342 ( 20%)
> failed                                             27844 ( 77%)
---- Errors --------------------------------------------------------------------
> j.n.ConnectException: connection timed out: localhost/127.0.0.  25648 (92.11%)
1:8989
> j.u.c.TimeoutException: Request timeout to localhost/127.0.0.1   2196 ( 7.89%)
:8989 after 60000 ms
================================================================================

### twirl

200 -> 200-400 -> 400 

================================================================================
---- Global Information --------------------------------------------------------
> request count                                      10699 (OK=8570   KO=2129  )
> min response time                                      0 (OK=158    KO=0     )
> max response time                                  54746 (OK=54746  KO=0     )
> mean response time                                  5486 (OK=6849   KO=0     )
> std deviation                                       9745 (OK=10451  KO=0     )
> response time 50th percentile                        758 (OK=1637   KO=0     )
> response time 75th percentile                       5691 (OK=8071   KO=0     )
> response time 95th percentile                      29759 (OK=32960  KO=0     )
> response time 99th percentile                      40634 (OK=42069  KO=0     )
> mean requests/sec                                159.687 (OK=127.91 KO=31.776)
---- Response Time Distribution ------------------------------------------------
> t < 800 ms                                          3299 ( 31%)
> 800 ms < t < 1200 ms                                 395 (  4%)
> t > 1200 ms                                         4876 ( 46%)
> failed                                              2129 ( 20%)
---- Errors --------------------------------------------------------------------
> j.n.ConnectException: connection timed out: localhost/127.0.0.   2129 (100.0%)
1:8989
================================================================================

1000 -> 1000-2000 -> 2000 

================================================================================
---- Global Information --------------------------------------------------------
> request count                                      36000 (OK=8680   KO=27320 )
> min response time                                      0 (OK=311    KO=0     )
> max response time                                  59644 (OK=59644  KO=0     )
> mean response time                                  4300 (OK=17833  KO=0     )
> std deviation                                      10595 (OK=14974  KO=0     )
> response time 50th percentile                          0 (OK=13849  KO=0     )
> response time 75th percentile                          0 (OK=28216  KO=0     )
> response time 95th percentile                      31089 (OK=46861  KO=0     )
> response time 99th percentile                      47214 (OK=55643  KO=0     )
> mean requests/sec                                    450 (OK=108.5  KO=341.5 )
---- Response Time Distribution ------------------------------------------------
> t < 800 ms                                          1218 (  3%)
> 800 ms < t < 1200 ms                                 207 (  1%)
> t > 1200 ms                                         7255 ( 20%)
> failed                                             27320 ( 76%)
---- Errors --------------------------------------------------------------------
> j.n.ConnectException: connection timed out: localhost/127.0.0.  25835 (94.56%)
1:8989
> j.u.c.TimeoutException: Request timeout to localhost/127.0.0.1   1485 ( 5.44%)
:8989 after 60000 ms
================================================================================

### jmustache

200 -> 200-400 -> 400 

================================================================================
---- Global Information --------------------------------------------------------
> request count                                       7200 (OK=5246   KO=1954  )
> min response time                                      0 (OK=2      KO=0     )
> max response time                                   8318 (OK=8318   KO=0     )
> mean response time                                  1608 (OK=2207   KO=0     )
> std deviation                                       1923 (OK=1937   KO=0     )
> response time 50th percentile                        753 (OK=1850   KO=0     )
> response time 75th percentile                       2750 (OK=3386   KO=0     )
> response time 95th percentile                       5769 (OK=6068   KO=0     )
> response time 99th percentile                       6796 (OK=7225   KO=0     )
> mean requests/sec                                232.258 (OK=169.226 KO=63.032)
---- Response Time Distribution ------------------------------------------------
> t < 800 ms                                          1798 ( 25%)
> 800 ms < t < 1200 ms                                 234 (  3%)
> t > 1200 ms                                         3214 ( 45%)
> failed                                              1954 ( 27%)
---- Errors --------------------------------------------------------------------
> j.n.ConnectException: Connection refused: no further informati    988 (50,56%)
on: localhost/127.0.0.1:8989
> j.n.ConnectException: Connection refused: no further informati    966 (49,44%)
on: localhost/0:0:0:0:0:0:0:1:8989
================================================================================
